<?php

namespace IbanqApiClient;

use IbanqApiClient\Exception\ConfigException;
use Illuminate\Support\ServiceProvider;

/**
 * Class IbanqServiceProvider
 * @package IbanqApiClient
 */
class IbanqServiceProvider extends ServiceProvider
{
    const BASE_URL_SANDBOX = 'https://sandbox.connect.ibanq.com';
    const BASE_URL_PRODUCTION = 'https://connect.ibanq.com';

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Determine if this is a Lumen application.
     *
     * @return bool
     */
    protected function isLumen()
    {
        return str_contains($this->app->version(), 'Lumen');
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!$this->isLumen()) {
            $this->publishes([
                __DIR__.'/Config/ibanq.php' => config_path('ibanq.php'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IbanqApi::class, function () {
            $api = new IbanqApi();

            if (!$env = \Config::get('ibanq.environment')) {
                throw new ConfigException('Ibanq environment not configured');
            }

            $api->setApiPath($this->getURL($env))
                ->setSecret(\Config::get('ibanq.secret'))
				->setClientId(\Config::get('ibanq.clientId'))
                ->setDebugMode(\Config::get('ibanq.debug'));

            return $api;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [IbanqApi::class];
    }

    /**
     * Return the appropriate API URL based on the environment.
     *
     * @param $environment
     *
     * @return string
     */
    public function getURL($environment)
    {
        try {
            return constant('self::BASE_URL_'.strtoupper($environment));
        } catch (\Exception $e) {
            throw new InvalidArgumentException('Ibanq environment should be one of "sandbox" or "production"');
        }
    }
}
