<?php
/**
 * Created by PhpStorm.
 * User: dayan
 * Date: 05/04/2021
 * Time: 21:45
 */

namespace IbanqApiClient\Api;

use IbanqApiClient\ApiClient;
use IbanqApiClient\Model\TransactionPageResponse;


class WalletsApi extends Api
{
	/**
	 * returns ballances in cents
	 *
	 * @param string $walletId
	 * @return array
	 */
	public function getBalances(string $walletId): array
	{
		$result = $this->getDetails($walletId);
		$balances = [];

		foreach ($result->getBalances() as $balance) {
			$balances[$balance->getCurrency()] = $balance->getAmount();
		}

		return $balances;
	}

	public function getDetails(string $walletId): \IbanqApiClient\Model\Wallet
	{
		//dump('[Ibanq]: getting wallet details');

		list($response, $statusCode, $httpHeader) = $this->getClient()->callApi(
			"/wallets/$walletId",
			ApiClient::GET,
			[],
			[],
			[],
			\IbanqApiClient\Model\Wallet::class,
			true
		);

		return $this->getClient()->getSerializer()->deserialize($response, \IbanqApiClient\Model\Wallet::class, $httpHeader);

	}

	/**
	 * @param string $walletId
	 * @param string $currency - ISO currency code
	 * @throws \IbanqApiClient\ApiException
	 * @return TransactionPageResponse
	 */
	public function getTransactionList(string $walletId, string $currency, array $query = []): TransactionPageResponse
	{
//		dump('[Ibanq]: getting wallet transactions');

		$validParams = [
			'amount',
			'amountFrom',
			'amountTo',
			'type',
			'date',
			'dateFrom',
			'dateTo',
			'otherParty',
			'reference',
			'offset',
			'limit',
			'sort[date]'
		];

		foreach ($query as $key => $val) {
			if (!isset($validParams[$key])) {
				throw new \Exception('Invalid param ' . $key);
			}
		}

		list($response, $code, $httpHeader) = $this->getClient()->callApi(
			"/wallets/$walletId/transactions/$currency",
			ApiClient::GET,
			[],
			[],
			[],
			\IbanqApiClient\Model\Transaction::class,
			true
		);

//		dump($response);

		return $this->getClient()->getSerializer()->deserialize($response, \IbanqApiClient\Model\TransactionPageResponse::class, $httpHeader);
	}
}