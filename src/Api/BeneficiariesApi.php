<?php
/**
 * Created by PhpStorm.
 * User: dayan
 * Date: 05/04/2021
 * Time: 12:51
 */

namespace IbanqApiClient\Api;
use IbanqApiClient\ApiClient;

class BeneficiariesApi extends Api
{

	const TYPE_INDIVIDUAL = 'individual';
	const TYPE_CORPORATE = 'corporate';

	private function validateType(string $type): void
	{
		if (!in_array($type, [
			self::TYPE_INDIVIDUAL,
			self::TYPE_CORPORATE
		])) {
			throw new \Exception('Unknown benefeciary type: ' . $type);
		}
	}

	/**
	 * @param string $beneficiaryId - uuid
	 * @param array $data
	 * @return mixed
	 * @throws \IbanqApiClient\ApiException
	 */
	public function addAccount(string $beneficiaryId, array $data)
	{

		$validParams = [
			'currency',  // "GBP"
			'accountHolder',  // "Mr J and Mrs J Smith"
			'nickname', // "John and Jane - Joint Account"
			'accountNumber', // 10510627
			'iban', // "GB06ABBY09022210510627"
			'sortCode', // 90222
			'swiftBic', // "ABBYGB2LXXX"
			'defaultReference', // "Salary payment"
			'bankGiro', // "123-4567"
			'aba', // 133563585
			'ach', // 623852453
			'bsb', // "939-200"
			'institutionNumber', // 567
			'bankCode', // 2345
			'branchCode', // 13
			'ifsc', // "ICIC0001359"
			'routingInstructions' // "No route"
		];

		foreach ($data as $key => $value) {
			if (!in_array($key, $validParams)) {
				throw new \Exception('Unknown param: ' . $key);
			}
		}

		list($result, $code, $headers) = $this->getClient()->callApi(
			'/beneficiaries/' . $beneficiaryId . '/accounts',
			ApiClient::POST,
			[],
			json_encode($data),
			[]
		);

		if ($code !== 201) {
			dump($result);
		}

		return $result->id;
	}

	public function getAccount(string $beneficiaryId)
	{
		list($result, $code, $headers) = $this->getClient()->callApi(
			'/beneficiaries/' . $beneficiaryId . '/accounts',
			ApiClient::GET,
			[],
			[],
			[]
		);

		if ($code !== 201) {
			dump($result);
		}

		return $result;
	}

	public function getList(array $query = array())
	{
	//	dump('[Ibanq]: retrieving b list');

		list($result, $code, $headers) = $this->getClient()->callApi(
			'/beneficiaries',
			ApiClient::GET,
			[],
			$query,
			[]
		);

		dump($result);
	}

	/**
	 * Should return required fields for benefeciary creation
	 *
	 * @param string $type
	 * @param string $country
	 * @param null|string $currency
	 * @throws \IbanqApiClient\ApiException
	 */
	public function getReference(string $type, string $country, ?string $currency = null)
	{
		$this->validateType($type);

		/**
		 * Get references for currency
		 * https://docs.connect.ibanq.com/#operation/viewBeneficiaryRulesForSpecificCurrency
		 */
		if ($currency) {
			list($result, $code, $headers) = $this->getClient()->callApi(
				"/reference/rules/beneficiary/$type/$country/$currency",
				ApiClient::GET,
				[],
				[],
				[]
			);

			return [
				'result' => $result,
				'code' => $code
			];
		}

		/**
		 * Get reference by country
		 * https://docs.connect.ibanq.com/#operation/viewBeneficiaryRulesForDefaultCurrency
		 */

		list($result, $code, $headers) = $this->getClient()->callApi(
			"/reference/rules/beneficiary/$type/$country",
			ApiClient::GET,
			[],
			[],
			[]
		);
	}

	/**
	 * {
	 *
	 * "firstNames": "Beneficiary's first names",
	 * "lastName": "Beneficiary's last name",
	 * "type": "individual",
	 * "uniqueReference": "uniqueReference",
	 * "phoneNumber": "111 222 333",
	 * "email": "some@email.com",
	 * "address":
	 *
	 * {
	 * "addressLine1": "119 Marylebone Road",
	 * "addressLine2": "Marylebone",
	 * "city": "London",
	 * "postcode": "NW1 5PU",
	 * "country": "UK"
	 * }
	 *
	 * }
	 *
	 *
	 *
	 *
	 * @param array $data
	 * @throws ApiException
	 */
	public function createBeneficiary(string $cid, array $data)
	{

		if (!isset($data['type'])) {
			throw new \Exception('Type not set creating Ibanq beneficiary');
		}

		/**
		 * Kostil to make it look the same as Modulr
		 */
		$type = strtolower($data['type']);

		$this->validateType($type);

		$data['customerReference'] = $cid;

		$required = [
			self::TYPE_INDIVIDUAL =>
				[
					'type',
					'address',
//					'email',
					'uniqueReference',
					'customerReference',
//					'phoneNumber',
					'lastName'
				],
			self::TYPE_CORPORATE => [
				'type',
				'address',
//				'email',
				'uniqueReference',
				'customerReference',
//				'phoneNumber',
				'name'
			]
		];

		foreach ($required[$type] as $key) {
			if (!isset($data[$key])) {
				throw new \Exception('Required param not set: ' . $key);
			}
		}

		list($result, $code, $headers) = $this->getClient()->callApi(
			'/beneficiaries',
			ApiClient::POST,
			[],
			json_encode($data),
			[],
			\IbanqApiClient\Model\Beneficiary::class
		);

		if ($code === 201) {

			$data['id'] = $result->id;

			return [
				$this->getClient()->getSerializer()->deserialize($data, \IbanqApiClient\Model\Beneficiary::class, $headers),
				$code,
				$headers
			];
		}

		return false;
	}

	public function updateBeneficiary(string $bid, array $data)
	{
		list($result, $code, $headers) = $this->getClient()->callApi(
			'/beneficiaries/' . $bid,
			ApiClient::PATCH,
			[],
			json_encode($data),
			[],
			\IbanqApiClient\Model\Beneficiary::class
		);

		if ($code === 204) {

			return [
				$this->getClient()->getSerializer()->deserialize($data, \IbanqApiClient\Model\Beneficiary::class, $headers),
				$code,
				$headers
			];
		}

		return false;
	}

	/**
	 *
	 * @param string $bid
	 * @throws ApiException
	 */
	public function approveBeneficiary(string $bid){

		list($result, $code, $headers) = $this->getClient()->callApi(

			'/beneficiaries/'.$bid.'/approve',
			ApiClient::POST,
			[],
			[],
			[]
		);

		if ($code === 204) {
			return [
				$this->getClient()->getSerializer()->deserialize($result, \IbanqApiClient\Model\Beneficiary::class, $headers),
				$code,
				$headers
			];
		}

		return false;
	}
	/**
	 *
	 * @param string $bid
	 * @throws ApiException
	 */
	public function rejectBeneficiary(string $bid, string $reason){

		list($result, $code, $headers) = $this->getClient()->callApi(

			'/beneficiaries/'.$bid.'/reject',
			ApiClient::POST,
			[],
			json_encode(['reason'=>$reason]),
			[]
		);

		if ($code === 204) {
			return [
				$code,
				$headers
			];
		}

		return false;
	}

	/**
	 *
	 * @param string $bid
	 * @throws ApiException
	 */
	public function approvalDetailsBeneficiary(string $bid){

		list($result, $code, $headers) = $this->getClient()->callApi(

			'/beneficiaries/'.$bid.'/approval',
			ApiClient::GET,
			[],
			[],
			[]
		);

		if ($code === 204) {
			return [
				$code,
				$headers
			];
		}

		return false;
	}

	/**
	 *
	 * @param string $bid
	 * @throws ApiException
	 */
	public function deleteBeneficiary(string $bid){

		list($result, $code, $headers) = $this->getClient()->callApi(

			'/beneficiaries/'.$bid,
			ApiClient::DELETE,
			[],
			[],
			[]
		);

		if ($code === 204) {
			return [
				$this->getClient()->getSerializer()->deserialize($result, \IbanqApiClient\Model\Beneficiary::class, $headers),
				$code,
				$headers
			];
		}

		return false;
	}
}
