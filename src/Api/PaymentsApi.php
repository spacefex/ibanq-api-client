<?php
/**
 * Created by PhpStorm.
 * User: dayan
 * Date: 05/04/2021
 * Time: 12:51
 */

namespace IbanqApiClient\Api;

use IbanqApiClient\ApiClient;

class PaymentsApi extends Api
{
	public function getList(array $query = array())
	{
		dump('[Ibanq]: transactions list');

		list($result, $code, $headers) = $this->getClient()->callApi(
			'/payments',
			ApiClient::GET,
			[],
			$query,
			[]
		);

		dump($result);
	}

	public function getByAccount(string $uuid)
	{
		return $this->getList([
			'account' => $uuid
		]);
	}

	/**
	 * Can return paymnt id or false in case of error
	 *
	 * @param array $data
	 * @return mixed
	 * @throws \IbanqApiClient\ApiException
	 */
	public function createPayment(array $data)
	{

		$required = ['accountId', 'amount', 'currency', 'reference'];

		foreach ($data as $key => $val) {
			if (!isset($data[$key])) {
				throw new \Exception('Required param not provided: ' . $key);
			}
		}

		list($result, $code, $headers) = $this->getClient()->callApi(
			'/payments',
			ApiClient::POST,
			[],
			json_encode($data),
			[]
		);

		if ($code !== 201) {
			return false;
		}

		return $result->id;
	}

	/**
	 *
	 * @param string $payment_id
	 * @throws ApiException
	 */
	public function approvPayment(string $payment_id){

		list($result, $code, $headers) = $this->getClient()->callApi(

			'/payments/'.$payment_id.'/approve',
			ApiClient::POST,
			[],
			[],
			[]
		);

		return $code;
	}

	/**
	 *
	 * @param string $payment_id
	 * @param string $reason
	 * @throws ApiException
	 */
	public function rejectPayment(string $payment_id, string $reason){

		list($result, $code, $headers) = $this->getClient()->callApi(

			'/payments/'.$payment_id.'/reject',
			ApiClient::POST,
			[],
			json_encode(['reason'=>$reason]),
			[]
		);

		return $code;
	}
}
