<?php
/**
 * Created by PhpStorm.
 * User: dayan
 * Date: 05/04/2021
 * Time: 12:54
 */

namespace IbanqApiClient\Api;

use IbanqApiClient\ApiClient;

abstract class Api
{
	/**
	 * API Client.
	 *
	 * @var ApiClient instance of the ApiClient
	 */
	protected $apiClient;

	/**
	 * Constructor.
	 *
	 * @param  ApiClient|null $apiClient The api client to use
	 */
	public function __construct(ApiClient $apiClient = null)
	{
		if ($apiClient === null) {
			$apiClient = new ApiClient();
		}

		$this->apiClient = $apiClient;
	}

	protected function getClient(): ApiClient
	{
		return $this->apiClient;
	}
}