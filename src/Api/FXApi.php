<?php
/**
 * Created by PhpStorm.
 * User: dayan
 * Date: 05/04/2021
 * Time: 12:51
 */

namespace IbanqApiClient\Api;

use IbanqApiClient\ApiClient;

class FXApi extends Api
{
	public function requetQuotes(array $query = array())
	{
		list($result, $code, $headers) = $this->getClient()->callApi(
			'/quotes',
			ApiClient::POST,
			[],
			json_encode($query),
			[]
		);

		return $result;
	}

	public function getQuotes(string $qid)
	{

		list($result, $code, $headers) = $this->getClient()->callApi(

			'/quotes/'.$qid,
			ApiClient::GET,
			[],
			[],
			[]
		);
		return $result;
	}

	/**
	 * Can return paymnt id or false in case of error
	 *
	 * @param array $data
	 * @return mixed
	 * @throws \IbanqApiClient\ApiException
	 */
	public function acceptQuotes(string $qid): mixed
	{
		list($result, $code, $headers) = $this->getClient()->callApi(

			'/quotes/'.$qid.'/accept',
			ApiClient::PATCH,
			[],
			[],
			[]
		);
		return $result;
	}

	/**
	 *
	 * @param string $payment_id
	 * @throws ApiException
	 */
	public function geTrades(string $trade_id){

		list($result, $code, $headers) = $this->getClient()->callApi(

			'/trades/'.$trade_id,
			ApiClient::GET,
			[],
			[],
			[]
		);

		return $result;
	}
}
