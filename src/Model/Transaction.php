<?php

namespace IbanqApiClient\Model;

use ArrayAccess;

/**
 * Class Transaction
 * @package IbanqApiClient\Model
 */
class Transaction extends Model implements ArrayAccess
{
	/**
	 * Array of property to type mappings. Used for (de)serialization.
	 *
	 * @var string[]
	 */
	protected static $swaggerTypes = [
		'id' => 'string',
		'date' => '\DateTime',
		'type' => 'string',
		'amount' => '\IbanqApiClient\Model\Balance',
		'other_party' => 'string',
		'reference' => 'string'
	];

	const DISCRIMINATOR = null;

	/**
	 * The original name of the model.
	 *
	 * @var string
	 */
	protected static $swaggerModelName = 'Transaction';

	public static function swaggerTypes()
	{
		return self::$swaggerTypes;
	}

	/**
	 * Array of attributes where the key is the local name, and the value is the original name.
	 *
	 * @var string[]
	 */
	protected static $attributeMap = [
		'id' => 'id',
		'date' => 'date',
		'type' => 'type',
		'amount' => 'amount',
		'other_party' => 'otherParty',
		'reference' => 'reference'
	];

	/**
	 * Array of attributes to setter functions (for deserialization of responses).
	 *
	 * @var string[]
	 */
	protected static $setters = [
		'id' => 'setId',
		'date' => 'setDate',
		'type' => 'setType',
		'amount' => 'setAmount',
		'other_party' => 'setOtherParty',
		'reference' => 'setReference'
	];

	/**
	 * Array of attributes to getter functions (for serialization of requests).
	 *
	 * @var string[]
	 */
	protected static $getters = [
		'id' => 'getId',
		'date' => 'getDate',
		'type' => 'getType',
		'amount' => 'getAmount',
		'other_party' => 'getOtherParty',
		'reference' => 'getReference'
	];


	const TYPE_CREDIT = 'credit';
	const TYPE_DEBIT = 'debit';

	/**
	 * Gets allowable values of the enum.
	 *
	 * @return string[]
	 */
	public function getTypeAllowableValues()
	{
		return [
			self::TYPE_CREDIT,
			self::TYPE_DEBIT
		];
	}

	/**
	 * Associative array for storing property values.
	 *
	 * @var mixed[]
	 */
	protected $container = [];

	public function __construct(array $data = null)
	{
		parent::__construct($data);
	}

	/**
	 * show all the invalid properties with reasons.
	 *
	 * @return array invalid properties with reasons
	 */
	public function listInvalidProperties()
	{
		$invalid_properties = [];

		if (!in_array($this->container['type'], $this->getTypeAllowableValues())) {
			$invalid_properties[] = sprintf(
				"invalid value for 'type', must be one of %s",
				implode(', ', $this->getTypeAllowableValues())
			);
		}

		return $invalid_properties;
	}

	/**
	 * validate all the properties in the model
	 * return true if all passed.
	 *
	 * @return bool True if all properties are valid
	 */
	public function valid()
	{
		if (!in_array($this->container['type'], $this->getTypeAllowableValues())) {
			return false;
		}

		return true;
	}

	/**
	 * Gets id.
	 *
	 * @return string
	 */
	public function getId()
	{
		return $this->container['id'];
	}

	/**
	 * Sets id.
	 *
	 * @param string $id Unique id for the Transaction
	 *
	 * @return $this
	 */
	public function setId($id)
	{
		$this->container['id'] = $id;

		return $this;
	}

	/**
	 * Gets amount.
	 *
	 */
	public function getAmount(): Balance
	{
		return $this->container['amount'];
	}

	/**
	 * Sets amount.
	 *
	 * @param Balance $amount Amount of the transaction in Major Currency Units
	 *
	 * @return $this
	 */
	public function setAmount(Balance $amount)
	{
		$this->container['amount'] = $amount;

		return $this;
	}

	/**
	 * Gets date
	 *
	 * @return \DateTime
	 */
	public function getDate(): \DateTime
	{
		return $this->container['date'];
	}

	/**
	 * Sets date.
	 *
	 * @param \DateTime $date Datetime when the transaction was posted to the Modulr system.
	 *
	 * @return $this
	 */
	public function setDate(\DateTime $date)
	{
		$this->container['date'] = $date;

		return $this;
	}

	public function getReference(): ?string
	{
		return $this->container['reference'];
	}

	public function setReference(?string $ref): self
	{
		$this->container['reference'] = $ref;

		return $this;
	}

	public function setOtherParty(?string $otherParty): self
	{
		$this->container['other_party'] = $otherParty;

		return $this;
	}


	public function getOtherParty(): ?string
	{
		return $this->container['other_party'];
	}

	/**
	 * Gets type.
	 *
	 * @return string
	 */
	public function getType(): string
	{
		return $this->container['type'];
	}

	/**
	 * Sets type.
	 *
	 * @param string $type Enumerated type indicating the type of the transaction. Values:
	 *
	 * @return $this
	 */
	public function setType(string $type)
	{
		$allowed_values = $this->getTypeAllowableValues();
		if (!in_array($this->container['type'], $allowed_values)) {
			$invalid_properties[] = sprintf(
				"invalid value for 'type', must be one of %s",
				implode(', ', $this->getTypeAllowableValues())
			);
		}

		$this->container['type'] = $type;

		return $this;
	}

	/**
	 * Gets the string presentation of the object.
	 *
	 * @return string
	 */
	public function __toString()
	{
		if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
			return json_encode(\IbanqApiClient\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
		}

		return json_encode(\IbanqApiClient\ObjectSerializer::sanitizeForSerialization($this));
	}
}
