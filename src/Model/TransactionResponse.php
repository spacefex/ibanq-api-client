<?php

namespace IbanqApiClient\Model;

use ArrayAccess;

/**
 * Class TransactionResponse
 * @package IbanqApiClient\Model
 */
class TransactionResponse extends Model implements ArrayAccess
{
	const DISCRIMINATOR = null;

	/**
	 * The original name of the model.
	 *
	 * @var string
	 */
	protected static $swaggerModelName = 'TransactionResponse';

	/**
	 * Array of property to type mappings. Used for (de)serialization.
	 *
	 * @var string[]
	 */
	protected static $swaggerTypes = [
		'content' => '\IbanqApiClient\Model\Transaction[]',
		'meta' => '\IbanqApiClient\Model\PageMeta',
	];

	public static function swaggerTypes()
	{
		return self::$swaggerTypes;
	}

	/**
	 * Array of attributes where the key is the local name, and the value is the original name.
	 *
	 * @var string[]
	 */
	protected static $attributeMap = [
		'content' => 'data',
		'meta' => 'meta'
	];

	/**
	 * Array of attributes to setter functions (for deserialization of responses).
	 *
	 * @var string[]
	 */
	protected static $setters = [
		'content' => 'setContent',
		'meta' => 'setMeta'
	];

	/**
	 * Array of attributes to getter functions (for serialization of requests).
	 *
	 * @var string[]
	 */
	protected static $getters = [
		'content' => 'getContent',
		'meta' => 'getMeta'
	];

	/**
	 * show all the invalid properties with reasons.
	 *
	 * @return array invalid properties with reasons
	 */
	public function listInvalidProperties()
	{
		$invalid_properties = [];

		return $invalid_properties;
	}

	/**
	 * validate all the properties in the model
	 * return true if all passed.
	 *
	 * @return bool True if all properties are valid
	 */
	public function valid()
	{
		return true;
	}

	/**
	 * Gets content.
	 *
	 * @return \ModulrApiClient\Model\Transaction[]
	 */
	public function getContent()
	{
		return $this->container['content'];
	}

	/**
	 * Sets content.
	 *
	 * @param \ModulrApiClient\Model\Transaction[] $content List of responses on the current page
	 *
	 * @return $this
	 */
	public function setContent($content)
	{
		$this->container['content'] = $content;

		return $this;
	}

	/**
	 * Gets page.
	 */
	public function getMeta(): PageMeta
	{
		return $this->container['meta'];
	}

	/**
	 * Sets page.
	 *
	 * @param PageMeta $meta
	 *
	 * @return $this
	 */
	public function setMeta(PageMeta $meta)
	{
		$this->container['meta'] = $meta;

		return $this;
	}


	/**
	 * Gets the string presentation of the object.
	 *
	 * @return string
	 */
	public function __toString()
	{
		if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
			return json_encode(\IbanqApiClient\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
		}

		return json_encode(\IbanqApiClient\ObjectSerializer::sanitizeForSerialization($this));
	}
}
