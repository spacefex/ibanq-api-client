<?php

namespace IbanqApiClient\Model;

use ArrayAccess;

/**
 * Class Beneficiary
 * @package IbanqApiClient\Model
 */
class Beneficiary extends Model implements ArrayAccess
{
	const DISCRIMINATOR = null;

	/**
	 * The original name of the model.
	 *
	 * @var string
	 */
	protected static $swaggerModelName = 'Beneficiary';

	/**
	 * Array of property to type mappings. Used for (de)serialization.
	 *
	 * @var string[]
	 */
	protected static $swaggerTypes = [
		'type' => 'string',
		'first_name' => 'string',
		'last_name' => 'string',
		'unique_reference' => 'string',
		'email' => 'string',
		'address' => \IbanqApiClient\Model\Address::class,
		'customer_reference' => 'string',
		'id' => 'string'
	];


	/**
	 * Array of attributes where the key is the local name, and the value is the original name.
	 *
	 * @var string[]
	 */
	protected static $attributeMap = [
		'type' => 'type',
		'first_name' => 'firstNames',
		'last_name' => 'lastName',
		'unique_reference' => 'uniqueReference',
		'email' => 'email',
		'address' => 'address',
		'customer_reference' => 'customerReference',
		'id' => 'id'
	];

	/**
	 * Array of attributes to setter functions (for deserialization of responses).
	 *
	 * @var string[]
	 */
	protected static $setters = [
		'type' => 'setType',
		'first_name' => 'setFirstName',
		'last_name' => 'setLastName',
		'unique_reference' => 'setUniqueReference',
		'email' => 'setEmail',
		'address' => 'setAddress',
		'customer_reference' => 'setCustomerReference',
		'id' => 'setId',
	];

	/**
	 * Array of attributes to getter functions (for serialization of requests).
	 *
	 * @var string[]
	 */
	protected static $getters = [
		'type' => 'getType',
		'first_name' => 'getFirstName',
		'last_name' => 'getLastName',
		'unique_reference' => 'getUniqueReference',
		'email' => 'getEmail',
		'address' => 'getAddress',
		'customer_reference' => 'getCustomerReference',
		'id' => 'getId',
	];

	const STATUS_ACTIVE = 'ACTIVE';
	const STATUS_DELETED = 'DELETED';
	const STATUS_PENDING = 'PENDING';
	const STATUS_BLOCKED = 'BLOCKED';

	/**
	 * Gets allowable values of the enum.
	 *
	 * @return string[]
	 */
	public function getStatusAllowableValues()
	{
		return [
			self::STATUS_ACTIVE,
			self::STATUS_DELETED,
			self::STATUS_PENDING,
			self::STATUS_BLOCKED,
		];
	}

	
	/**
	 * show all the invalid properties with reasons.
	 *
	 * @return array invalid properties with reasons
	 */
	public function listInvalidProperties()
	{
		$invalid_properties = [];

		if ($this->container['created'] === null) {
			$invalid_properties[] = "'created' can't be null";
		}
		if ($this->container['customer_id'] === null) {
			$invalid_properties[] = "'customer_id' can't be null";
		}
		if ($this->container['default_reference'] === null) {
			$invalid_properties[] = "'default_reference' can't be null";
		}
		if ($this->container['destination_identifier'] === null) {
			$invalid_properties[] = "'destination_identifier' can't be null";
		}
		if ($this->container['id'] === null) {
			$invalid_properties[] = "'id' can't be null";
		}
		if ($this->container['name'] === null) {
			$invalid_properties[] = "'name' can't be null";
		}
		if ($this->container['status'] === null) {
			$invalid_properties[] = "'status' can't be null";
		}
		$allowed_values = ['ACTIVE', 'DELETED', 'PENDING', 'BLOCKED'];
		if (!in_array($this->container['status'], $allowed_values)) {
			$invalid_properties[] = "invalid value for 'status', must be one of 'ACTIVE', 'DELETED', 'PENDING', 'BLOCKED'.";
		}

		return $invalid_properties;
	}

	/**
	 * validate all the properties in the model
	 * return true if all passed.
	 *
	 * @return bool True if all properties are valid
	 */
	public function valid()
	{
		if ($this->container['created'] === null) {
			return false;
		}
		if ($this->container['customer_id'] === null) {
			return false;
		}
		if ($this->container['default_reference'] === null) {
			return false;
		}
		if ($this->container['destination_identifier'] === null) {
			return false;
		}
		if ($this->container['id'] === null) {
			return false;
		}
		if ($this->container['name'] === null) {
			return false;
		}
		if ($this->container['status'] === null) {
			return false;
		}
		$allowed_values = ['ACTIVE', 'DELETED', 'PENDING', 'BLOCKED'];
		if (!in_array($this->container['status'], $allowed_values)) {
			return false;
		}

		return true;
	}


	public function getType(): string
	{
		return $this->container['type'];
	}

	public function setType(string $type): self
	{
		$this->container['type'] = $type;

		return $this;
	}

	/**
	 * Gets account_id.
	 *
	 * @return string
	 */
	public function getAccountId()
	{
		return $this->container['account_id'];
	}

	/**
	 * Sets account_id.
	 *
	 * @param string $account_id Id of the account if this beneficiary is a Modulr account, null otherwise
	 *
	 * @return $this
	 */
	public function setAccountId($account_id)
	{
		$this->container['account_id'] = $account_id;

		return $this;
	}

	/**
	 * Gets approval_required.
	 *
	 * @return bool
	 */
	public function getApprovalRequired()
	{
		return $this->container['approval_required'];
	}

	/**
	 * Sets approval_required.
	 *
	 * @param bool $approval_required Indicates if the beneficiary creation is pending approval
	 *
	 * @return $this
	 */
	public function setApprovalRequired($approval_required)
	{
		$this->container['approval_required'] = $approval_required;

		return $this;
	}

	/**
	 * Gets created.
	 *
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->container['created'];
	}

	/**
	 * Sets created.
	 *
	 * @param \DateTime $created Datetime the Beneficiary was created
	 *
	 * @return $this
	 */
	public function setCreated($created)
	{
		$this->container['created'] = $created;

		return $this;
	}

	/**
	 * Gets default_reference.
	 *
	 * @return string
	 */
	public function getDefaultReference()
	{
		return $this->container['default_reference'];
	}

	/**
	 * Sets default_reference.
	 *
	 * @param string $default_reference Default reference used for payments to the Beneficiary.
	 *
	 * @return $this
	 */
	public function setDefaultReference($default_reference)
	{
		$this->container['default_reference'] = $default_reference;

		return $this;
	}

	/**
	 * Gets address.
	 */
	public function getAddress(): \IbanqApiClient\Model\Address
	{
		return $this->container['address'];
	}

	/**
	 * Sets address.
	 */
	public function setAddress(\IbanqApiClient\Model\Address $address): self
	{
		$this->container['address'] = $address;

		return $this;
	}

	/**
	 * Gets destination_identifier.
	 *
	 * @return \ModulrApiClient\Model\AccountIdentifier
	 */
	public function getDestinationIdentifier()
	{
		return $this->container['destination_identifier'];
	}

	/**
	 * Sets destination_identifier.
	 *
	 * @param \ModulrApiClient\Model\AccountIdentifier $destination_identifier
	 *
	 * @return $this
	 */
	public function setDestinationIdentifier($destination_identifier)
	{
		$this->container['destination_identifier'] = $destination_identifier;

		return $this;
	}

	/**
	 * Gets external_reference.
	 *
	 * @return string
	 */
	public function getExternalReference()
	{
		return $this->container['external_reference'];
	}

	/**
	 * Sets external_reference.
	 *
	 * @param string $external_reference External system reference for the Beneficiary
	 *
	 * @return $this
	 */
	public function setExternalReference($external_reference)
	{
		$this->container['external_reference'] = $external_reference;

		return $this;
	}

	/**
	 * Gets id.
	 *
	 * @return string
	 */
	public function getId()
	{
		return $this->container['id'];
	}

	/**
	 * Sets id.
	 *
	 * @param string $id Unique reference for the Beneficiary. Begins with 'B'
	 *
	 * @return $this
	 */
	public function setId($id)
	{
		$this->container['id'] = $id;

		return $this;
	}

	/**
	 * Gets name.
	 *
	 * @return string
	 */
	public function getFirstName()
	{
		return $this->container['first_name'];
	}

	/**
	 * Sets name.
	 *
	 * @param string $name Name for the Beneficiary
	 *
	 * @return $this
	 */
	public function setFirstName($name)
	{
		$this->container['first_name'] = $name;

		return $this;
	}

	/**
	 * Gets name.
	 *
	 * @return string
	 */
	public function getLastName()
	{
		return $this->container['last_name'];
	}

	/**
	 * Sets name.
	 *
	 * @param string $name Name for the Beneficiary
	 *
	 * @return $this
	 */
	public function setLastName($name)
	{
		$this->container['last_name'] = $name;

		return $this;
	}

	/**
	 * Gets status.
	 *
	 * @return string
	 */
	public function getStatus()
	{
		return $this->container['status'];
	}


	public function setUniqueReference(string $ref)
	{
		$this->container['unique_reference'] = $ref;

		return $this;
	}

	public function getUniqueReference(): string
	{
		return $this->container['unique_reference'];
	}

	public function setEmail(string $email): self
	{
		$this->container['email'] = $email;

		return $this;
	}

	public function getEmail(): string
	{
		return $this->container['email'];
	}

	public function getCustomerReference(): string
	{
		return $this->container['customer_reference'];
	}

	public function setCustomerReference(string $ref): self
	{
		$this->container['customer_reference'] = $ref;

		return $this;
	}

	/**
	 * Sets status.
	 *
	 * @param string $status Status of the Beneficiary. Can be:
	 *
	 * @return $this
	 */
	public function setStatus($status)
	{
		$allowed_values = ['ACTIVE', 'DELETED', 'PENDING', 'BLOCKED'];
		if ((!in_array($status, $allowed_values))) {
			throw new \InvalidArgumentException("Invalid value for 'status', must be one of 'ACTIVE', 'DELETED', 'PENDING', 'BLOCKED'");
		}
		$this->container['status'] = $status;

		return $this;
	}

	/**
	 * Returns true if offset exists. False otherwise.
	 *
	 * @param int $offset Offset
	 *
	 * @return bool
	 */
	public function offsetExists($offset)
	{
		return isset($this->container[$offset]);
	}

	/**
	 * Gets offset.
	 *
	 * @param int $offset Offset
	 *
	 * @return mixed
	 */
	public function offsetGet($offset)
	{
		return isset($this->container[$offset]) ? $this->container[$offset] : null;
	}

	/**
	 * Sets value based on offset.
	 *
	 * @param int $offset Offset
	 * @param mixed $value Value to be set
	 *
	 * @return void
	 */
	public function offsetSet($offset, $value)
	{
		if (is_null($offset)) {
			$this->container[] = $value;
		} else {
			$this->container[$offset] = $value;
		}
	}

	/**
	 * Unsets offset.
	 *
	 * @param int $offset Offset
	 *
	 * @return void
	 */
	public function offsetUnset($offset)
	{
		unset($this->container[$offset]);
	}

	/**
	 * Gets the string presentation of the object.
	 *
	 * @return string
	 */
	public function __toString()
	{
		if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
			return json_encode(\IbanqApiClient\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
		}

		return json_encode(\IbanqApiClient\ObjectSerializer::sanitizeForSerialization($this));
	}
}
