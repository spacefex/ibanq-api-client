<?php
/**
 * Created by PhpStorm.
 * User: dayan
 * Date: 13/04/2021
 * Time: 00:33
 */

namespace IbanqApiClient\Model;

use ArrayAccess;
use DateTime;
use IbanqApiClient\ObjectSerializer;

class Balance extends Model implements ArrayAccess
{

	const DISCRIMINATOR = null;

	/**
	 * The original name of the model.
	 *
	 * @var string
	 */
	protected static $swaggerModelName = 'Balance';

	protected static $swaggerTypes = [
		'currency' => 'string',
		'amount' => 'float',
	];

	/**
	 * Array of attributes where the key is the local name, and the value is the original name.
	 *
	 * @var string[]
	 */
	protected static $attributeMap = [
		'currency' => 'currency',
		'amount' => 'amount',
	];

	protected static $setters = [
		'currency' => 'setCurrency',
		'amount' => 'setAmount',
	];

	protected static $getters = [
		'currency' => 'getCurrency',
		'amount' => 'getAmount',
	];

	/**
	 * validate all the properties in the model
	 * return true if all passed.
	 *
	 * @return bool True if all properties are valid
	 */
	public function valid()
	{
		return true;
	}

	public function getCurrency(): string
	{
		return $this->container['currency'];
	}

	public function setCurrency(string $currency): self
	{
		$this->container['currency'] = $currency;

		return $this;
	}

	public function getAmount(): float
	{
		return $this->container['amount'];
	}

	public function setAmount(float $amount): self
	{
		$this->container['amount'] = $amount;

		return $this;
	}

}