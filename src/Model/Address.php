<?php

namespace IbanqApiClient\Model;

use ArrayAccess;

/**
 * Class Address
 * @package ModulrApiClient\Model
 */
class Address extends Model implements ArrayAccess
{
	const DISCRIMINATOR = null;

	/**
	 * The original name of the model.
	 *
	 * @var string
	 */
	protected static $swaggerModelName = 'Address';

	/**
	 * Array of property to type mappings. Used for (de)serialization.
	 *
	 * @var string[]
	 */
	protected static $swaggerTypes = [
		'address_line1' => 'string',
		'address_line2' => 'string',
		'country' => 'string',
		'post_code' => 'string',
		'post_town' => 'string',
	];

	/**
	 * Array of attributes where the key is the local name, and the value is the original name.
	 *
	 * @var string[]
	 */
	protected static $attributeMap = [
		'address_line1' => 'addressLine1',
		'address_line2' => 'addressLine2',
		'country' => 'country',
		'post_code' => 'postcode',
		'post_town' => 'city',
	];

	/**
	 * Array of attributes to setter functions (for deserialization of responses).
	 *
	 * @var string[]
	 */
	protected static $setters = [
		'address_line1' => 'setAddressLine1',
		'address_line2' => 'setAddressLine2',
		'country' => 'setCountry',
		'post_code' => 'setPostCode',
		'post_town' => 'setPostTown',
	];

	/**
	 * Array of attributes to getter functions (for serialization of requests).
	 *
	 * @var string[]
	 */
	protected static $getters = [
		'address_line1' => 'getAddressLine1',
		'address_line2' => 'getAddressLine2',
		'country' => 'getCountry',
		'post_code' => 'getPostCode',
		'post_town' => 'getPostTown',
	];

	/**
	 * show all the invalid properties with reasons.
	 *
	 * @return array invalid properties with reasons
	 */
	public function listInvalidProperties()
	{
		$invalid_properties = [];

		if ($this->container['address_line1'] === null) {
			$invalid_properties[] = "'address_line1' can't be null";
		}
		if ($this->container['country'] === null) {
			$invalid_properties[] = "'country' can't be null";
		}
		if ($this->container['id'] === null) {
			$invalid_properties[] = "'id' can't be null";
		}
		if ($this->container['post_code'] === null) {
			$invalid_properties[] = "'post_code' can't be null";
		}
		if ($this->container['post_town'] === null) {
			$invalid_properties[] = "'post_town' can't be null";
		}

		return $invalid_properties;
	}

	/**
	 * validate all the properties in the model
	 * return true if all passed.
	 *
	 * @return bool True if all properties are valid
	 */
	public function valid()
	{
		if ($this->container['address_line1'] === null) {
			return false;
		}
		if ($this->container['country'] === null) {
			return false;
		}
		if ($this->container['id'] === null) {
			return false;
		}
		if ($this->container['post_code'] === null) {
			return false;
		}
		if ($this->container['post_town'] === null) {
			return false;
		}

		return true;
	}

	/**
	 * Gets address_line1
	 */
	public function getAddressLine1(): ?string
	{
		return $this->container['address_line1'];
	}

	/**
	 * Sets address_line1.
	 */
	public function setAddressLine1(?string $address_line1): self
	{
		$this->container['address_line1'] = $address_line1;

		return $this;
	}

	/**
	 * Gets address_line2
	 */
	public function getAddressLine2(): ?string
	{
		return $this->container['address_line2'];
	}

	/**
	 * Sets address_line2
	 */
	public function setAddressLine2(?string $address_line2): self
	{
		$this->container['address_line2'] = $address_line2;

		return $this;
	}

	/**
	 * Gets country.
	 *
	 * @return string
	 */
	public function getCountry(): string
	{
		return $this->container['country'];
	}

	/**
	 * Sets country.
	 */
	public function setCountry(string $country): self
	{
		$this->container['country'] = $country;

		return $this;
	}

	/**
	 * Gets post_code.
	 *
	 * @return string
	 */
	public function getPostCode()
	{
		return $this->container['post_code'];
	}

	/**
	 * Sets post_code.
	 *
	 * @param string $post_code
	 *
	 * @return $this
	 */
	public function setPostCode($post_code)
	{
		$this->container['post_code'] = $post_code;

		return $this;
	}

	/**
	 * Gets post_town.
	 *
	 * @return string
	 */
	public function getPostTown()
	{
		return $this->container['post_town'];
	}

	/**
	 * Sets post_town.
	 *
	 * @param string $post_town
	 *
	 * @return $this
	 */
	public function setPostTown($post_town)
	{
		$this->container['post_town'] = $post_town;

		return $this;
	}

	/**
	 * Returns true if offset exists. False otherwise.
	 *
	 * @param int $offset Offset
	 *
	 * @return bool
	 */
	public function offsetExists($offset)
	{
		return isset($this->container[$offset]);
	}

	/**
	 * Gets offset.
	 *
	 * @param int $offset Offset
	 *
	 * @return mixed
	 */
	public function offsetGet($offset)
	{
		return isset($this->container[$offset]) ? $this->container[$offset] : null;
	}

	/**
	 * Sets value based on offset.
	 *
	 * @param int $offset Offset
	 * @param mixed $value Value to be set
	 *
	 * @return void
	 */
	public function offsetSet($offset, $value)
	{
		if (is_null($offset)) {
			$this->container[] = $value;
		} else {
			$this->container[$offset] = $value;
		}
	}

	/**
	 * Unsets offset.
	 *
	 * @param int $offset Offset
	 *
	 * @return void
	 */
	public function offsetUnset($offset)
	{
		unset($this->container[$offset]);
	}

	/**
	 * Gets the string presentation of the object.
	 *
	 * @return string
	 */
	public function __toString()
	{
		if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
			return json_encode(\IbanqApiClient\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
		}

		return json_encode(\IbanqApiClient\ObjectSerializer::sanitizeForSerialization($this));
	}
}
