<?php

namespace IbanqApiClient\Model;

use ArrayAccess;

/**
 * Class TransactionResponse
 * @package IbanqApiClient\Model
 */
class PageMeta extends Model implements ArrayAccess
{
	const DISCRIMINATOR = null;

	/**
	 * The original name of the model.
	 *
	 * @var string
	 */
	protected static $swaggerModelName = 'PageMeta';

	/**
	 * Array of property to type mappings. Used for (de)serialization.
	 *
	 * @var string[]
	 */
	protected static $swaggerTypes = [
		// ??
		'count' => 'int',

		// The number of items to skip before starting to collect the result set. This, combined with the limit parameter can be used to group your results into pages where offset = (page number - 1) * limit
		'offset' => 'int',

		//The number of items to list in the result set - combine this with the offset parameter to page your results.
		'limit' => 'int',

		'total' => 'double'
	];

	public static function swaggerTypes()
	{
		return self::$swaggerTypes;
	}

	/**
	 * Array of attributes where the key is the local name, and the value is the original name.
	 *
	 * @var string[]
	 */
	protected static $attributeMap = [
		'count' => 'count',
		'offset' => 'offset',
		'limit' => 'limit',
		'total' => 'total',
	];

	/**
	 * Array of attributes to setter functions (for deserialization of responses).
	 *
	 * @var string[]
	 */
	protected static $setters = [
		'count' => 'setCount',
		'offset' => 'setOffset',
		'limit' => 'setLimit',
		'total' => 'setTotal',
	];

	/**
	 * Array of attributes to getter functions (for serialization of requests).
	 *
	 * @var string[]
	 */
	protected static $getters = [
		'count' => 'getCount',
		'offset' => 'getOffset',
		'limit' => 'getLimit',
		'total' => 'getTotal',
	];


	/**
	 * show all the invalid properties with reasons.
	 *
	 * @return array invalid properties with reasons
	 */
	public function listInvalidProperties()
	{
		$invalid_properties = [];

		return $invalid_properties;
	}

	/**
	 * validate all the properties in the model
	 * return true if all passed.
	 *
	 * @return bool True if all properties are valid
	 */
	public function valid()
	{
		return true;
	}

	/**
	 * Gets page.
	 *
	 * @return int
	 */
	public function getCount(): int
	{
		return (int)$this->container['count'];
	}

	/**
	 * Sets page.
	 *
	 * @param int $count Current page number. Its 0 based. i.e firstpage =0, secondpage=1
	 *
	 * @return $this
	 */
	public function setCount($count): self
	{
		$this->container['count'] = $count;

		return $this;
	}

	/**
	 * Gets offset
	 *
	 * @return int
	 */
	public function getOffset(): int
	{
		return (int)$this->container['offset'];
	}

	/**
	 * Sets offset.
	 *
	 * @param int $offset
	 *
	 * @return $this
	 */
	public function setOffset(int $offset)
	{
		$this->container['offset'] = $offset;

		return $this;
	}

	/**
	 * Gets size.
	 *
	 * @return int
	 */
	public function getLimit(): int
	{
		return is_null($this->container['limit']) ? 1 : $this->container['limit'];
	}

	/**
	 * Sets size.
	 *
	 * @param int $limit Page size
	 *
	 * @return $this
	 */
	public function setLimit(int $limit)
	{
		$this->container['limit'] = $limit;

		return $this;
	}

	/**
	 * Gets total_pages.
	 *
	 * @return int
	 */
	public function getTotal()
	{
		return $this->container['total'];
	}

	/**
	 * Sets total_pages.
	 *
	 * @param int $total Total pages
	 *
	 * @return $this
	 */
	public function setTotal(int $total): self
	{
		$this->container['total'] = $total;

		return $this;
	}

	/**
	 * Gets the string presentation of the object.
	 *
	 * @return string
	 */
	public function __toString()
	{
		if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
			return json_encode(\IbanqApiClient\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
		}

		return json_encode(\IbanqApiClient\ObjectSerializer::sanitizeForSerialization($this));
	}
}
