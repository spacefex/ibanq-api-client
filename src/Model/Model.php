<?php
/**
 * Created by PhpStorm.
 * User: dayan
 * Date: 29/04/2021
 * Time: 00:43
 */

namespace IbanqApiClient\Model;

class Model
{

	protected static $attributeMap = [];
	protected static $setters;
	protected static $getters;

	/**
	 * @return array
	 */
	public static function setters()
	{
		return static::$setters;
	}

	/**
	 * @return array
	 */
	public static function getters()
	{
		return static::$getters;
	}

	/**
	 * @return array
	 */
	public static function swaggerTypes()
	{
		return static::$swaggerTypes;
	}

	/**
	 * Associative array for storing property values.
	 *
	 * @var mixed[]
	 */
	protected $container = [];

	/**
	 * Constructor.
	 *
	 * @param mixed[] $data Associated array of property values initializing the model
	 */
	public function __construct(array $data = null)
	{
		foreach (static::$attributeMap as $key => $value) {
			$this->container[$key] = isset($data[$key]) ? $data[$key] : null;
		}
	}

	/**
	 * show all the invalid properties with reasons.
	 *
	 * @return array invalid properties with reasons
	 */
	public function listInvalidProperties()
	{
		$invalid_properties = [];

		return $invalid_properties;
	}

	/**
	 * @return array
	 */
	public static function attributeMap($attribute = null)
	{
		if ($attribute) {

			if (!isset(static::$attributeMap[$attribute])) {
				throw new \Exception('Attribute not found: ' . $attribute);
			}

			return static::$attributeMap[$attribute];
		}

		return static::$attributeMap;
	}

	public static function findLocalAttribute($search)
	{
		foreach (static::$attributeMap as $local => $remote) {
			if ($remote == $search) {
				return $local;
			}
		}

		throw new \Exception('Local attribute map not found for ' . $search);
	}

	public function offsetSet($offset, $value)
	{
		if (is_null($offset)) {
			$this->container[] = $value;
		} else {
			$this->container[$offset] = $value;
		}
	}

	public function offsetExists($offset)
	{
		return isset($this->container[$offset]);
	}

	public function offsetUnset($offset)
	{
		unset($this->container[$offset]);
	}

	public function offsetGet($offset)
	{
		return isset($this->container[$offset]) ? $this->container[$offset] : null;
	}

	public function __toArray(): array
	{
		$result = [];

		foreach ($this->getters() as $prop => $func) {
			$result[$prop] = $this->{$func}();
//			$result[$prop] = $func;
		}

		return $result;
	}
}