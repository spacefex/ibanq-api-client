<?php

namespace IbanqApiClient\Model;

use ArrayAccess;
use DateTime;
use ModulrApiClient\ObjectSerializer;

/**
 * Class Account
 * @package ModulrApiClient\Model
 */
class Account implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
     * The original name of the model.
     *
     * @var string
     */
    protected static $swaggerModelName = 'Account';

    /**
     * Array of property to type mappings. Used for (de)serialization.
     *
     * @var string[]
     */
    protected static $swaggerTypes = [
        'balance'            => 'string',
        'available_balance'  => 'string',
        'created_date'       => '\DateTime',
        'currency'           => 'string',
        'customer_id'        => 'string',
        'external_reference' => 'string',
        'id'                 => 'string',
        'identifiers'        => '\ModulrApiClient\Model\AccountIdentifier[]',
        'name'               => 'string',
        'status'             => 'string',
    ];

    /**
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name.
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'balance'            => 'balance',
        'available_balance'  => 'availableBalance',
        'created_date'       => 'createdDate',
        'currency'           => 'currency',
        'customer_id'        => 'customerId',
        'external_reference' => 'externalReference',
        'id'                 => 'id',
        'identifiers'        => 'identifiers',
        'name'               => 'name',
        'status'             => 'status',
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses).
     *
     * @var string[]
     */
    protected static $setters = [
        'balance'            => 'setBalance',
        'available_balance'  => 'setAvailableBalance',
        'created_date'       => 'setCreatedDate',
        'currency'           => 'setCurrency',
        'customer_id'        => 'setCustomerId',
        'external_reference' => 'setExternalReference',
        'id'                 => 'setId',
        'identifiers'        => 'setIdentifiers',
        'name'               => 'setName',
        'status'             => 'setStatus',
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests).
     *
     * @var string[]
     */
    protected static $getters = [
        'balance'            => 'getBalance',
        'available_balance'  => 'getAvailableBalance',
        'created_date'       => 'getCreatedDate',
        'currency'           => 'getCurrency',
        'customer_id'        => 'getCustomerId',
        'external_reference' => 'getExternalReference',
        'id'                 => 'getId',
        'identifiers'        => 'getIdentifiers',
        'name'               => 'getName',
        'status'             => 'getStatus',
    ];

    /**
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_INACTIVE = 'INACTIVE';
    const STATUS_BLOCKED = 'BLOCKED';
    const STATUS_CLOSED = 'CLOSED';

    /**
     * Gets allowable values of the enum.
     *
     * @return string[]
     */
    public function getStatusAllowableValues()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_INACTIVE,
            self::STATUS_BLOCKED,
            self::STATUS_CLOSED,
        ];
    }

    /**
     * Associative array for storing property values.
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor.
     *
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
		foreach (self::$attributeMap as $key)
		{
			$this->container[$key] = isset($data[$key]) ? $data[$key] : null;
		}
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if ($this->container['balance'] === null) {
            $invalid_properties[] = "'balance' can't be null";
        }
        if ($this->container['created_date'] === null) {
            $invalid_properties[] = "'created_date' can't be null";
        }
        if ($this->container['currency'] === null) {
            $invalid_properties[] = "'currency' can't be null";
        }
        if ($this->container['customer_id'] === null) {
            $invalid_properties[] = "'customer_id' can't be null";
        }
        if ($this->container['id'] === null) {
            $invalid_properties[] = "'id' can't be null";
        }
        if ($this->container['identifiers'] === null) {
            $invalid_properties[] = "'identifiers' can't be null";
        }
        if ($this->container['name'] === null) {
            $invalid_properties[] = "'name' can't be null";
        }
        if ($this->container['status'] === null) {
            $invalid_properties[] = "'status' can't be null";
        }
        $allowed_values = ['ACTIVE', 'INACTIVE', 'BLOCKED', 'CLOSED'];
        if (!in_array($this->container['status'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'status', must be one of 'ACTIVE', 'INACTIVE', 'BLOCKED', 'CLOSED'.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed.
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        if ($this->container['balance'] === null) {
            return false;
        }
        if ($this->container['created_date'] === null) {
            return false;
        }
        if ($this->container['currency'] === null) {
            return false;
        }
        if ($this->container['customer_id'] === null) {
            return false;
        }
        if ($this->container['id'] === null) {
            return false;
        }
        if ($this->container['identifiers'] === null) {
            return false;
        }
        if ($this->container['name'] === null) {
            return false;
        }
        if ($this->container['status'] === null) {
            return false;
        }
        $allowed_values = ['ACTIVE', 'INACTIVE', 'BLOCKED', 'CLOSED'];
        if (!in_array($this->container['status'], $allowed_values)) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getBalance()
    {
        return $this->container['balance'];
    }

    /**
     * @param string $balance Balance of the account in format 'NN.NN'
     *
     * @return Account
     */
    public function setBalance($balance)
    {
        $this->container['balance'] = $balance;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvailableBalance()
    {
        return $this->container['available_balance'];
    }

    /**
     * @param $availableBalance
     *
     * @return Account
     */
    public function setAvailableBalance($availableBalance)
    {
        $this->container['available_balance'] = $availableBalance;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->container['created_date'];
    }

    /**
     * @param DateTime $created_date Datetime when the account was created
     *
     * @return Account
     */
    public function setCreatedDate($created_date)
    {
        $this->container['created_date'] = $created_date;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->container['currency'];
    }

    /**
     * @param string $currency Currency of the account in ISO 4217 format
     *
     * @return Account
     */
    public function setCurrency($currency)
    {
        $this->container['currency'] = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId()
    {
        return $this->container['customer_id'];
    }

    /**
     * @param string $customer_id Unique id of the Customer
     *
     * @return Account
     */
    public function setCustomerId($customer_id)
    {
        $this->container['customer_id'] = $customer_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalReference()
    {
        return $this->container['external_reference'];
    }

    /**
     * @param string $external_reference Your reference for an account
     *
     * @return Account
     */
    public function setExternalReference($external_reference)
    {
        $this->container['external_reference'] = $external_reference;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * @param string $id Unique id for the account
     *
     * @return Account
     */
    public function setId($id)
    {
        $this->container['id'] = $id;
        return $this;
    }

    /**
     * @return AccountIdentifier[]
     */
    public function getIdentifiers()
    {
        return $this->container['identifiers'];
    }

    /**
     * @return AccountIdentifier
     */
    public function getFirstIdentifier()
    {
        return $this->getIdentifiers()[0];
    }

    /**
     * @param AccountIdentifier[] $identifiers
     *
     * @return Account
     */
    public function setIdentifiers($identifiers)
    {
        $this->container['identifiers'] = $identifiers;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * @param string $name Name for the account
     *
     * @return Account
     */
    public function setName($name)
    {
        $this->container['name'] = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * @param string $status Status of the account. Accounts must be 'ACTIVE' to make and receive payments. Can be one of
     *
     * @return Account
     */
    public function setStatus($status)
    {
        $allowed_values = ['ACTIVE', 'INACTIVE', 'BLOCKED', 'CLOSED'];
        if ((!in_array($status, $allowed_values))) {
            throw new \InvalidArgumentException("Invalid value for 'status', must be one of 'ACTIVE', 'INACTIVE', 'BLOCKED', 'CLOSED'");
        }
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * @param int $offset Offset
     *
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * @param int $offset Offset
     *
     * @return int|null
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * @param int   $offset Offset
     * @param mixed $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * @param int $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) {
            return json_encode(ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
