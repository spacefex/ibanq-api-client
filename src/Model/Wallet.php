<?php

namespace IbanqApiClient\Model;

use ArrayAccess;
use DateTime;
use IbanqApiClient\ObjectSerializer;

/**
 * Class Account
 * @package IbanqApiClient\Model
 */
class Wallet extends Model implements ArrayAccess
{
	public function __construct(array $data = null)
	{
		parent::__construct($data);
	}

	/**
	 * Array of property to type mappings. Used for (de)serialization.
	 *
	 * @var string[]
	 */
	protected static $swaggerTypes = [
		'account_name' => 'string',
		'iban' => 'string',
		'swift_bic' => 'string',
		'account_number' => 'int',
		'sort_code' => 'int',
		'balances' => '\IbanqApiClient\Model\Balance[]',

		'id' => 'string',
	];

	const DISCRIMINATOR = null;

	/**
	 * The original name of the model.
	 *
	 * @var string
	 */
	protected static $swaggerModelName = 'Wallet';

	/**
	 * Array of attributes where the key is the local name, and the value is the original name.
	 *
	 * @var string[]
	 */
	public static $attributeMap = [
		'account_name' => 'accountName',
		'iban' => 'iban',
		'swift_bic' => 'swiftBic',
		'account_number' => 'accountNumber',
		'sort_code' => 'sortCode',
		'balances' => 'balances',

		'id' => 'id',
	];

	/**
	 * Array of attributes to setter functions (for deserialization of responses).
	 *
	 * @var string[]
	 */
	protected static $setters = [
		'account_name' => 'setAccountName',
		'iban' => 'setIban',
		'swift_bic' => 'setSwiftBic',
		'account_number' => 'setAccountNumber',
		'sort_code' => 'setSortCode',
		'balances' => 'setBalances',

		'id' => 'setId',
	];

	/**
	 * Array of attributes to getter functions (for serialization of requests).
	 *
	 * @var string[]
	 */
	protected static $getters = [
		'account_name' => 'getAccountName',
		'iban' => 'getIban',
		'swift_bic' => 'getSwiftBic',
		'account_number' => 'getAccountNumber',
		'sort_code' => 'getSortCode',
		'balances' => 'getBalances',

		'id' => 'getId',
	];

	/**
	 * validate all the properties in the model
	 * return true if all passed.
	 *
	 * @return bool True if all properties are valid
	 */
	public function valid()
	{
		return true;
	}

	/**
	 * @return string
	 */
	public function getAccountName(): ?string
	{
		return $this->container['account_name'];
	}

	/**
	 * @param string $balance Balance of the account in format 'NN.NN'
	 *
	 * @return Wallet
	 */
	public function setAccountName(string $accountName): self
	{
		$this->container['account_name'] = $accountName;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getIban(): string
	{
		return $this->container['iban'];
	}

	/**
	 * @param $availableBalance
	 *
	 * @return Wallet
	 */
	public function setIban(string $iban): self
	{
		$this->container['iban'] = $iban;

		return $this;
	}


	public function setSwiftBic(string $swift): self
	{
		$this->container['swift_bic'] = $swift;

		return $this;
	}

	public function getSwiftBic(): string
	{
		return $this->container['swift_bic'];
	}

	public function setAccountNumber(string $accountNumber): self
	{
		$this->container['account_number'] = $accountNumber;

		return $this;
	}

	public function getAccountNumber(): string
	{
		return $this->container['account_number'];
	}

	public function getSortCode(): string
	{
		return $this->container['sort_code'];
	}

	public function setSortCode(string $sortCode): self
	{
		$this->container['sort_code'] = $sortCode;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getId(): string
	{
		return $this->container['id'];
	}

	/**
	 * @param string $id Unique id for the account
	 *
	 * @return Wallet
	 */
	public function setId($id): self
	{
		$this->container['id'] = $id;

		return $this;
	}


	/**
	 * @param Balance[] $balances
	 *
	 */
	public function setBalances($balances): self
	{
		$this->container['balances'] = $balances;

		return $this;
	}

	/**
	 * @return Balance[]
	 */
	public function getBalances(): array
	{
		return $this->container['balances'];
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		if (defined('JSON_PRETTY_PRINT')) {
			return json_encode(ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
		}

		return json_encode(ObjectSerializer::sanitizeForSerialization($this));
	}
}
