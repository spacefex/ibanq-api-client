<?php

namespace IbanqApiClient;

/**
 * Class ApiClient
 * @package IbanqApiClient
 */
class ApiClient
{
	public const PATCH = 'PATCH';
	public const POST = 'POST';
	public const GET = 'GET';
	public const HEAD = 'HEAD';
	public const OPTIONS = 'OPTIONS';
	public const PUT = 'PUT';
	public const DELETE = 'DELETE';
 
	/**
	 * Configuration.
	 *
	 * @var Configuration
	 */
	protected $config;

	protected $serializer;

	/**
	 * @var string
	 */
	protected $accessToken;

	public function setAccessToken(string $accessToken): self
	{
		$this->accessToken = $accessToken;

		return $this;
	}

	/**
	 * @var string
	 */
	protected $refreshToken;

	/**
	 * @param string $refreshToken
	 * @return ApiClient
	 */
	public function setRefreshToken(string $refreshToken): self
	{
		$this->refreshToken = $refreshToken;

		return $this;
	}

	public function getSerializer()
	{
		return $this->serializer;
	}

	/**
	 * Constructor of the class.
	 *
	 * @param  Configuration $config config for this ApiClient
	 */
	public function __construct(\IbanqApiClient\Configuration $config = null)
	{
		if ($config === null) {
			$config = Configuration::getDefaultConfiguration();
		}

		$this->config = $config;
		$this->serializer = new ObjectSerializer();
	}

	public function callApi(
		$resourcePath,
		$method,
		array $queryParams,
		$postData,
		$headerParams,
		$responseType = null,
		$assoc = false
	)
	{

		$headers = [];

		// construct the http header
		$headerParams = array_merge(
			(array)$this->config->getDefaultHeaders(),
			(array)$headerParams
		);

		foreach ($headerParams as $key => $val) {
			$headers[] = "$key: $val";
		}

		/**
		 * If we are authenticated - use header
		 */
		if ($this->accessToken) {
			$headers[] = 'Authorization: Bearer ' . $this->accessToken;
		}

		$url = $this->config->getHost() . $resourcePath;

		$curl = curl_init();
		// set timeout, if needed
		if ($this->config->getCurlTimeout() !== 0) {
			curl_setopt($curl, CURLOPT_TIMEOUT, $this->config->getCurlTimeout());
		}
		// set connect timeout, if needed
		if ($this->config->getCurlConnectTimeout() != 0) {
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->config->getCurlConnectTimeout());
		}

		// return the result on success, rather than just true
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		// disable SSL verification, if needed
		if ($this->config->getSSLVerification() === false) {
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		}

		if ($this->config->getCurlProxyHost()) {
			curl_setopt($curl, CURLOPT_PROXY, $this->config->getCurlProxyHost());
		}

		if ($this->config->getCurlProxyPort()) {
			curl_setopt($curl, CURLOPT_PROXYPORT, $this->config->getCurlProxyPort());
		}

		if ($this->config->getCurlProxyType()) {
			curl_setopt($curl, CURLOPT_PROXYTYPE, $this->config->getCurlProxyType());
		}

		if ($this->config->getCurlProxyUser()) {
			curl_setopt(
				$curl,
				CURLOPT_PROXYUSERPWD,
				$this->config->getCurlProxyUser() . ':' . $this->config->getCurlProxyPassword()
			);
		}

		if (!empty($queryParams)) {
			$url = ($url . '?' . http_build_query($queryParams));
		}

		if (gettype($postData) === 'string') {
//			dump($method . ': ' .  $url);
//			dump(json_decode($postData));
		}

		if ($method === self::POST) {
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
		} elseif ($method === self::HEAD) {
			curl_setopt($curl, CURLOPT_NOBODY, true);
		} elseif ($method === self::OPTIONS) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'OPTIONS');
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
		} elseif ($method === self::PATCH) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
		} elseif ($method === self::PUT) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
		} elseif ($method === self::DELETE) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
		} elseif ($method !== self::GET) {
			throw new ApiException('Method ' . $method . ' is not recognized.');
		}

//		dump($method . ': ' . $url);

		curl_setopt($curl, CURLOPT_URL, $url);

		// debugging for curl
		if ($this->config->getDebug()) {
			error_log(
				'[DEBUG] HTTP Request body  ~BEGIN~' . PHP_EOL . print_r($postData, true) . PHP_EOL . '~END~' . PHP_EOL,
				3,
				$this->config->getDebugFile()
			);
			curl_setopt($curl, CURLOPT_VERBOSE, 1);
			curl_setopt($curl, CURLOPT_STDERR, fopen($this->config->getDebugFile(), 'a'));
		} else {
			curl_setopt($curl, CURLOPT_VERBOSE, 0);
		}

		// obtain the HTTP response headers
		curl_setopt($curl, CURLOPT_HEADER, 1);

		// Make the request
		$response = curl_exec($curl);
		$http_header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$http_header = $this->httpParseHeaders(substr($response, 0, $http_header_size));
		$http_body = substr($response, $http_header_size);
		$response_info = curl_getinfo($curl);

		//dump($response);

		// debug HTTP response body
		if ($this->config->getDebug()) {
			error_log(
				'[DEBUG] HTTP Response body ~BEGIN~' . PHP_EOL . print_r($http_body, true) . PHP_EOL . '~END~' . PHP_EOL,
				3,
				$this->config->getDebugFile()
			);
		}

// Handle the response
		if ($response_info['http_code'] === 0) {

			$curl_error_message = curl_error($curl);

			// curl_exec can sometimes fail but still return a blank message from curl_error().
			if (!empty($curl_error_message)) {
				$error_message = "API call to $url failed: $curl_error_message";
			} else {
				$error_message = "API call to $url failed, but for an unknown reason. " .
					'This could happen if you are disconnected from the network.';
			}

			$exception = new ApiException($error_message, 0, null, null);
			$exception->setResponseObject($response_info);

			throw $exception;

		} elseif ($response_info['http_code'] >= 200 && $response_info['http_code'] <= 299) {
			// return raw body if response is a file
			if ($responseType === '\SplFileObject' || $responseType === 'string') {
				return [$http_body, $response_info['http_code'], $http_header];
			}

			$data = json_decode($http_body, $assoc);

			if (json_last_error() > 0) { // if response is a string
				$data = $http_body;
			}
		} else {

			//dump($http_body);

			$data = json_decode($http_body);

			if (json_last_error() > 0) { // if response is a string
				$data = $http_body;
			}

			//dump($data);

			throw new ApiException(
				'[' . $response_info['http_code'] . "] Error connecting to the API ($url)",
				$response_info['http_code'],
				$http_header,
				$data
			);
		}

		return [$data, $response_info['http_code'], $http_header];
	}

	/**
	 * Return an array of HTTP response headers.
	 *
	 * @param  string $raw_headers A string of raw HTTP response headers
	 *
	 * @return string[] Array of HTTP response heaers
	 */
	protected function httpParseHeaders($raw_headers)
	{
		// ref/credit: http://php.net/manual/en/function.http-parse-headers.php#112986
		$headers = [];
		$key = '';

		foreach (explode("\n", $raw_headers) as $h) {
			$h = explode(':', $h, 2);

			if (isset($h[1])) {
				if (!isset($headers[$h[0]])) {
					$headers[$h[0]] = trim($h[1]);
				} elseif (is_array($headers[$h[0]])) {
					$headers[$h[0]] = array_merge($headers[$h[0]], [trim($h[1])]);
				} else {
					$headers[$h[0]] = array_merge([$headers[$h[0]]], [trim($h[1])]);
				}

				$key = $h[0];
			} else {
				if (substr($h[0], 0, 1) === "\t") {
					$headers[$key] .= "\r\n\t" . trim($h[0]);
				} elseif (!$key) {
					$headers[0] = trim($h[0]);
				}
				trim($h[0]);
			}
		}

		return $headers;
	}

	public function auth(string $username, string $password, string $clientId, string $secret): bool
	{
		list($result, $code, $headers) = $this->callApi(
			'/oauth/token',
			ApiClient::POST,
			[],
			[
				'grant_type' => 'password',
				'client_id' => $clientId,
				'client_secret' => $secret,
				'username' => $username,
				'password' => $password,
			],
			[]
		);

		if ($code === 200) {
			$this->setAccessToken($result->access_token)
				->setRefreshToken($result->refresh_token);

//			dump('[Ibanq]: Authenticated');

			return true;
		}

		return false;
	}
}
