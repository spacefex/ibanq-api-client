<?php

return [
    'app_name' => env('IBANQ_API_NAME'),
    'client_id' => env('IBANQ_CLIENT_ID'),
    'secret' => env('IBANQ_SECRET'),
    'environment' => env('IBANQ_ENVIRONMENT', 'sandbox'),
    //'debug' => env('IBANQ_DEBUG', true),
	'redirect_url' => env('IBANQ_REDIRECT_URL')
];
