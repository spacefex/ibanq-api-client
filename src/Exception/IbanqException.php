<?php

namespace IbanqApiClient\Exception;

use Throwable;

interface IbanqException extends Throwable
{
}
