<?php

namespace IbanqApiClient\Exception;

use Exception;

class ConfigException extends Exception implements IbanqException
{
}
