<?php

namespace IbanqApiClient;

use Carbon\Carbon;
use IbanqApiClient\Api\AccountsApi;
use IbanqApiClient\Api\BeneficiariesApi;
use IbanqApiClient\Api\CustomersApi;
use IbanqApiClient\Api\DocumentUploadApi;
use IbanqApiClient\Api\FXApi;
use IbanqApiClient\Api\InboundpaymentsApi;
use IbanqApiClient\Api\NotificationsApi;
use IbanqApiClient\Api\PaymentsApi;
use IbanqApiClient\Api\RuleApi;
use IbanqApiClient\Api\TransactionsApi;
use IbanqApiClient\Api\WalletsApi;
use IbanqApiClient\Exception\ConfigException;

/**
 * Class IbanqApi
 * @package IbanqApiClient
 */
class IbanqApi
{
	/**
	 * @var ApiClient;
	 */
	private $client;

	/**
	 * @var
	 */
	private $nonce;

	/**
	 * @var
	 */
	private $date;

	/**
	 * @var string
	 */
	private $apiPath;

	/**
	 * @var string
	 */
	private $secret;

	/**
	 * @var string
	 */
	private $clientId;

	/**
	 * @var
	 */
	private $debugMode = false;

	/**
	 * @var
	 */
	private $timezone = 'UTC';

	/**
	 * IbanqApi constructor.
	 */
	public function __construct()
	{
	}

	/**
	 * Generates new nonce if not set
	 * Returns nonce.
	 *
	 * @return string
	 */
	public function getNonce()
	{
		if (is_null($this->nonce)) {
			$this->setNonce(str_replace('.', '-', uniqid(null, true)));
		}

		return $this->nonce;
	}

	/**
	 * Set nonce.
	 *
	 * @param $nonce
	 *
	 * @return $this
	 */
	public function setNonce($nonce)
	{
		$this->nonce = $nonce;

		return $this;
	}

	/**
	 * Generates date if not set
	 * Returns date.
	 *
	 * @return mixed
	 */
	public function getDate()
	{
		if (is_null($this->date)) {
			$this->setDate(Carbon::now($this->timezone));
		}

		return $this->date->format('D, d M Y H:i:s e');
	}

	/**
	 * Sets date.
	 *
	 * @param Carbon $date
	 *
	 * @return $this
	 */
	public function setDate(Carbon $date)
	{
		$this->date = $date;

		return $this;
	}

	/**
	 * Sets the timezone.
	 *
	 * @param Carbon $date
	 *
	 * @return $this
	 */
	public function setTimezone($timezone = 'UTC')
	{
		$this->timezone = $timezone;

		return $this;
	}

	/**
	 * Sets API Path.
	 *
	 * @param string $apiPath
	 *
	 * @return $this
	 */
	public function setApiPath($apiPath)
	{
		$this->apiPath = $apiPath;

		return $this;
	}

	/**
	 * Sets API Path.
	 *
	 * @param string $apiKey
	 *
	 * @return $this
	 */
	public function setSecret(string $secret): self
	{
		$this->secret = $secret;

		return $this;
	}

	/**
	 * @param string $clientId
	 * @return IbanqApi
	 */
	public function setClientId(string $clientId): self
	{
		$this->clientId = $clientId;

		return $this;
	}

	/**
	 * Sets Debug mode.
	 *
	 * @param string $debug
	 *
	 * @return $this
	 */
	public function setDebugMode($debug)
	{
		$this->debugMode = $debug;

		return $this;
	}

	/**
	 * Checks if Ibanq config is setup.
	 *
	 * @throws ConfigException
	 */
	private function checkConfig()
	{
		if (!$this->secret) {
			throw new ConfigException('Please set your IbanqFinance API key in the config file');
		}
	}

	/**
	 * @param  null $nonce
	 * @return ApiClient
	 * @throws ConfigException
	 */
	private function createClient($nonce = null)
	{
		$this->checkConfig();
		$config = new Configuration();

		$config->setSecret($this->secret);
		$config->setHost($this->apiPath);
		$config->setClientId($this->clientId);

		$config->setDebug($this->debugMode);

		$api = new ApiClient($config);

		return $api;
	}

	public function getClient(): ApiClient
	{
		if (!$this->client) {
			$this->client = $this->createClient();
		}

		return $this->client;
	}

	/**
	 * Authenticates client and gets token
	 *
	 * @param string $username
	 * @param string $password
	 * @return bool
	 * @throws ApiException
	 */
	public function authenticateClient(string $username, string $password): bool
	{
		return $this->getClient()->auth($username, $password, $this->clientId, $this->secret);
	}

	public function getBenefeciariesApi(): BeneficiariesApi
	{
		return new BeneficiariesApi($this->getClient());
	}

	public function getWalletsApi(): WalletsApi
	{
		return new WalletsApi($this->getClient());
	}

	public function getPaymentsApi(): PaymentsApi
	{
		return new PaymentsApi($this->getClient());
	}

	public function getFXApi(): FXApi
	{
		return new FXApi($this->getClient());
	}
}
