<?php

namespace IbanqApiClient\Facades;

use Illuminate\Support\Facades\Facade;
use IbanqApiClient\Api;

/**
 * Class IbanqApi
 * @package IbanqApiClient\Facades
 *
 * @method static Api\AccountsApi accounts(string $nonce = null)
 * @method static Api\BeneficiariesApi beneficiaries(string $nonce = null)
 * @method static Api\CustomersApi customers(string $nonce = null)
 * @method static Api\DocumentUploadApi documents(string $nonce = null)
 * @method static Api\InboundpaymentsApi inboundPayments(string $nonce = null)
 * @method static Api\NotificationsApi notifications(string $nonce = null)
 * @method static Api\PaymentsApi payments(string $nonce = null)
 * @method static Api\RuleApi rule(string $nonce = null)
 * @method static Api\TransactionsApi transactions(string $nonce = null)
 */
class IbanqApi extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'IbanqApiClient\IbanqApi';
    }
}
